<% local data, viewlibrary, page_info, session = ... %>
<% htmlviewfunctions = require("htmlviewfunctions") %>
<% html = require("acf.html") %>

<script type="text/javascript">
	if (typeof jQuery == 'undefined') {
		document.write('<script type="text/javascript" src="<%= html.html_escape(page_info.wwwprefix) %>/js/jquery-latest.js"><\/script>');
	}
</script>

<script type="text/javascript">
	if (typeof $.tablesorter == 'undefined') {
		document.write('<script type="text/javascript" src="<%= html.html_escape(page_info.wwwprefix) %>/js/jquery.tablesorter.js"><\/script>');
	}
</script>

<script type="text/javascript">
	$(function(){
		$("#usage").tablesorter({headers: {0:{sorter: 'shortDate'}, 1:{sorter: 'text'}, 2:{sorter: 'digit'}, 3:{sorter: 'digit'}}, widgets: ['zebra']});
	});
</script>

<% local subdata, pagedata = htmlviewfunctions.paginate(data.value, page_info.clientdata, 100) %>

<% local header_level = htmlviewfunctions.displaysectionstart(data, page_info) %>
<% htmlviewfunctions.displaypagination(pagedata, page_info) %>
<table id="usage" class="tablesorter">
<thead>
	<tr>
		<th>Date</th>
		<th>Source</th>
		<th>Requests</th>
		<th>Blocks</th>
	</tr>
</thead>
<tbody>
<% for i,stat in ipairs(subdata) do %>
	<tr>
		<td><%= html.html_escape(stat.date) %></td>
		<td><%= html.html_escape(stat.sourcename) %></td>
		<td><%= html.html_escape(stat.numrequest) %></td>
		<td><%= html.html_escape(stat.numblock) %></td>
	</tr>
<% end %>
</tbody>
</table>

<% if data.errtxt then %>
<p class='error'><%= html.html_escape(data.errtxt) %></p>
<% end %>
<% if #data.value == 0 then %>
<p>No usage stats found</p>
<% end %>
<% htmlviewfunctions.displaysectionend(header_level) %>
