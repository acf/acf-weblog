<% local data, viewlibrary, page_info, session = ... %>
<% htmlviewfunctions = require("htmlviewfunctions") %>
<% html = require("acf.html") %>

<script type="text/javascript">
	if (typeof jQuery == 'undefined') {
		document.write('<script type="text/javascript" src="<%= html.html_escape(page_info.wwwprefix) %>/js/jquery-latest.js"><\/script>');
	}
</script>

<script type="text/javascript">
	if (typeof $.tablesorter == 'undefined') {
		document.write('<script type="text/javascript" src="<%= html.html_escape(page_info.wwwprefix) %>/js/jquery.tablesorter.js"><\/script>');
	}
</script>

<script type="text/javascript">
	$(document).ready(function() {
		<% if data.value.groupby.value == "clientip" then %>
		$("#audit").tablesorter({headers: {0:{sorter:'ipAddress'}, 1:{sorter:'digit'}, 2:{sorter:'digit'}, 3:{sorter:'digit'}, 4:{sorter:'digit'}, 5:{sorter:'digit'}, 6:{sorter:'digit'}}, widgets: ['zebra']});
		<% else %>
		$("#audit").tablesorter({widgets: ['zebra']});
		<% end %>
	});
</script>

<% htmlviewfunctions.displaycommandresults({"completeaudit"}, session) %>

<% local header_level = htmlviewfunctions.displaysectionstart(cfe({label="Audit Parameters"}), page_info) %>
<% htmlviewfunctions.displayitem(data.value.auditstart) %>
<% htmlviewfunctions.displayitem(data.value.auditend) %>
<% htmlviewfunctions.displaysectionend(header_level) %>

<% htmlviewfunctions.displaysectionstart(data, page_info, header_level) %>
<table id="audit" class="tablesorter"><thead>
	<tr>
		<th><% if data.value.groupby.value == "clientip" then %>Client IP<% else %>User ID<% end %></th>
		<th>Total Requests</th>
		<th>Flagged Requests</th>
		<th>Total Score</th>
		<th>Blocked</th>
		<th>Overridden</th>
		<th>Maximum Score</th>
	</tr>
</thead><tbody>
<% for i,stat in ipairs(data.value.stats.value) do %>
	<tr><td><%= html.link{value = "viewweblog?"..data.value.groupby.value.."="..stat[data.value.groupby.value], label=stat[data.value.groupby.value]} %></td>
	<td><%= html.html_escape(stat.numrecords) %></td>
	<td><%= html.html_escape(stat.numflagged) %></td>
	<td><%= html.html_escape(stat.numhits) %></td>
	<td><%= html.html_escape(stat.numdenied) %></td>
	<td><%= html.html_escape(stat.numbypassed) %></td>
	<td><%= html.html_escape(stat.maxscore) %></td></tr>
<% end %>
</tbody></table>

<% if data.errtxt then %>
<p class='error'><%= html.html_escape(data.errtxt) %></p>
<% end %>
<% if #data.value.stats.value == 0 then %>
<p>No flagged records, try adjusting the audit dates</p>
<% end %>

<% htmlviewfunctions.displayitem(cfe({type="form", value={}, label="Complete Audit", option="Complete", action="completeaudit"}), page_info, 0) %>
<% htmlviewfunctions.displaysectionend(header_level) %>
