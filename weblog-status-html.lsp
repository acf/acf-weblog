<% local data, viewlibrary, page_info, session = ...
htmlviewfunctions = require("htmlviewfunctions")
%>

<% htmlviewfunctions.displaycommandresults({"createdatabase"}, session, true) %>

<% local header_level = htmlviewfunctions.displaysectionstart(cfe({label="Weblog Database Status"}), page_info) %>
<% local status
if viewlibrary and viewlibrary.dispatch_component then
	if viewlibrary.check_permission("postgresql/postgresql/status") then
		status = viewlibrary.dispatch_component("postgresql/postgresql/status", nil, true)
	end
end %>

<%
local statuscfe = cfe({ label="Database status" })
if status then
	statuscfe.value = status.value.status.errtxt or status.value.status.value
else
	statuscfe.value = "Unknown"
end
htmlviewfunctions.displayitem(statuscfe)
%>

<%
local databasecfe = cfe({ label="Weblog Database" })
if data.value then
	databasecfe.value = "Present"
else
	databasecfe.value = "Missing"
end
htmlviewfunctions.displayitem(databasecfe)
%>

<% htmlviewfunctions.displaysectionend(header_level) %>

<% if not data.value and viewlibrary and viewlibrary.dispatch_component then
	viewlibrary.dispatch_component("createdatabase")
end %>
