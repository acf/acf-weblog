<% local data, viewlibrary, page_info, session = ... %>
<% htmlviewfunctions = require("htmlviewfunctions") %>
<% html = require("acf.html") %>

<script type="text/javascript">
	if (typeof jQuery == 'undefined') {
		document.write('<script type="text/javascript" src="<%= html.html_escape(page_info.wwwprefix) %>/js/jquery-latest.js"><\/script>');
	}
</script>

<script type="text/javascript">
	if (typeof $.tablesorter == 'undefined') {
		document.write('<script type="text/javascript" src="<%= html.html_escape(page_info.wwwprefix) %>/js/jquery.tablesorter.js"><\/script>');
	}
</script>

<script type="text/javascript">
	$(document).ready(function() {
		$("#activity").tablesorter({headers: {0:{sorter: 'shortDate'}}, widgets: ['zebra']});
	});
</script>

<% local subdata, pagedata = htmlviewfunctions.paginate(data.value, page_info.clientdata, 100) %>

<% local header_level = htmlviewfunctions.displaysectionstart(data, page_info) %>
<% htmlviewfunctions.displaypagination(pagedata, page_info) %>
<table id="activity" class="tablesorter"><thead>
	<tr>
		<th>Date</th>
		<th>Message</th>
	</tr>
</thead><tbody>
<% for i,log in ipairs(subdata) do %>
	<tr>
		<td><%= html.html_escape(string.gsub(log.logdatetime, "%..*", "")) %></td>
		<td><%= html.html_escape(log.msgtext) %></td>
	</tr>
<% end %>
</tbody></table>

<% if #data.value == 0 then %>
<p>No history found</p>
<% end %>
<% htmlviewfunctions.displayinfo(data) %>

<% htmlviewfunctions.displaysectionend(header_level) %>
