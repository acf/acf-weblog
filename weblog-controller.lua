local mymodule = {}

mymodule.default_action = "viewauditstats"

function mymodule.config(self)
	return self.handle_form(self, self.model.getconfig, self.model.updateconfig, self.clientdata, "Save", "Edit Configuration", "Configuration Saved")
end

function mymodule.listsources(self)
	return self.model.getsourcelist()
end

function mymodule.createsource(self)
	return self.handle_form(self, self.model.getnewsource, self.model.createsource, self.clientdata, "Create", "Create new source", "New source created")
end

function mymodule.deletesource(self)
	return self.handle_form(self, self.model.getdeletesource, self.model.deletesource, self.clientdata, "Delete", "Delete source", "Source deleted")
end

function mymodule.editsource(self)
	return self.handle_form(self, function() return self.model.getsource(self.clientdata.sourcename) end, self.model.updatesource, self.clientdata, "Save", "Edit Source", "Source Saved")
end

function mymodule.testsource(self)
	return self.handle_form(self, self.model.gettestsource, self.model.testsource, self.clientdata, "Test", "Test Source")
end

function mymodule.importlogs(self)
	return self.handle_form(self, self.model.getimportlogs, self.model.importlogs, self.clientdata, "Import", "Import Logs")
end

function mymodule.viewactivitylog(self)
	return self.model.getactivitylog()
end

function mymodule.viewweblog(self)
	local retval = self.handle_form(self, self.model.getweblogparameters, self.model.getweblog, self.clientdata, "Update", "Display Weblog")
	-- We want to get the weblog even if form wasn't submitted
	if not self.clientdata.submit then
		retval = self.model.getweblog(self, retval)
	end
	return retval
end

function mymodule.downloadweblog(self)
	local retval = mymodule.viewweblog(self)
	local file = cfe({ type="raw", value="", label="Weblog-"..os.date()..".tab" })
	local content = {"sourcename\tclientuserid\tclientip\tlogdatetime\turi\tbytes\treason\tscore\tshortreason\tbadyesno\tdeniedyesno\tbypassyesno"}
	for i,log in ipairs(retval.value.log.value) do
		content[#content+1] = string.format("%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s",
			log.sourcename, log.clientuserid, log.clientip, log.logdatetime, log.uri, log.bytes, log.reason, log.score, log.shortreason or "", log.badyesno, log.deniedyesno, log.bypassyesno)
	end
	file.value = table.concat(content, "\n")
	retval.value.file = file
	return retval
end

function mymodule.updateselected(self)
	return self.handle_form(self, self.model.geteditselected, self.model.editselected, self.clientdata, "Submit", "Submit select update", "Select fields updated")
end

function mymodule.clearselected(self)
	return self.handle_form(self, self.model.getclearselected, self.model.clearselected, self.clientdata, "Clear", "Clear select fields", "Select fields cleared")
end

function mymodule.viewusagestats(self)
	return self.model.getusagestats()
end

function mymodule.viewauditstats(self)
	return self.model.getauditstats()
end

function mymodule.completeaudit(self)
	return self.handle_form(self, self.model.getcompleteaudit, self.model.completeaudit, self.clientdata, "Complete", "Complete Audit", "Audit completed")
end

function mymodule.adhocquery(self)
	return self.handle_form(self, self.model.getnewadhocquery, self.model.adhocquery, self.clientdata, "Submit", "Submit ad-hoc query")
end

function mymodule.downloadadhocquery(self)
	local retval = self.model.getnewadhocquery(self, self.clientdata)
	self.handle_clientdata(retval, self.clientdata)
	retval = self.model.adhocquery(self, retval)

	if retval.value.result and #retval.value.result.value > 0 then
		local file = cfe({ type="raw", value="", label="weblogadhocquery.tab" })
		local content = {table.concat(retval.value.names.value, "\t")}
		for i,entry in ipairs(retval.value.result.value) do
			local line = {}
			for i,name in ipairs(retval.value.names.value) do
				line[#line+1] = entry[name] or ""
			end
			content[#content+1] = table.concat(line, "\t")
		end
		file.value = table.concat(content, "\n")
		retval.value.file = file
	end

	return retval
end

function mymodule.status(self)
	return self.model.testdatabase()
end

function mymodule.createdatabase(self)
	return self.handle_form(self, self.model.getnewdatabase, self.model.create_database, self.clientdata, "Create", "Create New Database", "Database Created")
end

function mymodule.listfiles(self)
	return self.model.listfiles(self)
end

function mymodule.editfile(self)
	return self.handle_form(self, self.model.readfile, self.model.updatefile, self.clientdata, "Save", "Edit Weblog File", "Weblog File Saved" )
end

return mymodule
