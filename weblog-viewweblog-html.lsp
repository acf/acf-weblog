<% local data, viewlibrary, page_info, session = ... %>
<% htmlviewfunctions = require("htmlviewfunctions") %>
<% html = require("acf.html") %>
<% format = require("acf.format") %>

<%
-- Insert a string into another string
function string_insert(value, insert, place)
	if place == nil then
		place = string.len(value)+1
	end

	return string.sub(value, 1,place-1) .. tostring(insert) .. string.sub(value, place, string.len(value))
end

--Highlight occurences of a word in a string
function string_highlight(txtvalue, searchval, fcolour, bcolour)
	if txtvalue ~=nil and searchval ~= nil then
		sStart = string.find(string.lower(txtvalue),string.lower(searchval))
		if sStart ~= nil then
			sEnd = sStart + string.len(searchval)
			txtvalue = string_insert(txtvalue,"</font>", sEnd)
			txtvalue = string_insert(txtvalue,"<font style='color:"..fcolour.."; background-color:"..bcolour..";'>", sStart)
		end
	end

	return txtvalue
end
%>

<script type="text/javascript">
	if (typeof jQuery == 'undefined') {
		document.write('<script type="text/javascript" src="<%= html.html_escape(page_info.wwwprefix) %>/js/jquery-latest.js"><\/script>');
	}
</script>

<script type="text/javascript">
	if (typeof $.tablesorter == 'undefined') {
		document.write('<script type="text/javascript" src="<%= html.html_escape(page_info.wwwprefix) %>/js/jquery.tablesorter.js"><\/script>');
	}
</script>

<script type="text/javascript">
	function clickhead(e) {
		var selectdata = "";
		var deselectdata = "";
		var array=[];
		$(".chktbl").each(function () {
			array.push(this.id);
		});
		if (this.checked == false) {
			$('.chktbl:checked').prop('checked', false).closest('tr').removeClass("selected");
			deselectdata = array.join("%0D%0A");
		} else {
			$('.chktbl:not(:checked)').prop('checked', true).closest('tr').addClass("selected");
			selectdata = array.join("%0D%0A");
		}
		$.getJSON(
			'<%= html.html_escape(page_info.script .. page_info.prefix .. page_info.controller .. "/updateselected") %>?viewtype=json&submit=true&select='+selectdata+'&deselect='+deselectdata,
			function(data) {}
		);
	}

	function clicktable(e) {
		var selectdata = "";
		var deselectdata = "";
		if (this.checked == true) {
			$(this).closest('tr').addClass("selected");
			selectdata = this.id;
		} else {
			// If clearing, clear the Head too
			$("#chkHead").prop("checked", false);
			$(this).closest('tr').removeClass("selected");
			deselectdata = this.id;
		}
		$.getJSON(
			'<%= html.html_escape(page_info.script .. page_info.prefix .. page_info.controller .. "/updateselected") %>?viewtype=json&submit=true&select='+selectdata+'&deselect='+deselectdata,
			function(data) {}
		);
	}

	$(function(){
		$("#loglist").tablesorter({headers: {0:{sorter: false}, 1:{sorter: 'text'}}, widgets: ['zebra']});
		$('#chkHead').click(clickhead);
		$('.chktbl').click(clicktable);
<% if data.value.focus.value ~= "" then %>
		if ($("#focus").length) {
			var top = $("#focus").offset().top;
			$("html,body").scrollTop(top);
		}
<% end %>
	});
</script>

<% local subdata, pagedata = htmlviewfunctions.paginate(data.value.log.value, page_info.clientdata, 200) %>

<style type="text/css">
.tablesorter-blue tbody > tr.even.selected > td,
.tablesorter-blue tbody > tr.even.selected + tr.tablesorter-childRow > td,
.tablesorter-blue tbody > tr.even.selected + tr.tablesorter-childRow + tr.tablesorter-childRow > td {
	background: #FDAD31;
}
.tablesorter-blue tbody > tr.odd.selected > td,
.tablesorter-blue tbody > tr.odd.selected + tr.tablesorter-childRow > td,
.tablesorter-blue tbody > tr.odd.selected + tr.tablesorter-childRow + tr.tablesorter-childRow > td {
	background: #FC9A01;
}
.tablesorter-blue tbody > tr.even.selected:hover > td,
.tablesorter-blue tbody > tr.even.selected:hover + tr.tablesorter-childRow > td,
.tablesorter-blue tbody > tr.even.selected:hover + tr.tablesorter-childRow + tr.tablesorter-childRow > td {
	background: #D48101;
}
.tablesorter-blue tbody > tr.odd.selected:hover > td,
.tablesorter-blue tbody > tr.odd.selected:hover + tr.tablesorter-childRow > td,
.tablesorter-blue tbody > tr.odd.selected:hover + tr.tablesorter-childRow + tr.tablesorter-childRow > td {
	background: #CA7B01;
}
</style>

<% -- Display the form, but skip log, window, and focus fields
local label = data.label
data.label = "Search Parameters"
local log = data.value.log
data.value.log = nil
local window = data.value.window
data.value.window = nil
local focus = data.value.focus
data.value.focus = nil
htmlviewfunctions.displayitem(data, page_info)
data.label = label
data.value.log = log
data.value.window = window
data.value.focus = focus
%>

<%
local clientinfo = "submit=true&badyesno=false&deniedyesno=false&bypassyesno=false&selected=false&"
if data.value.clientuserid.value ~= "" then
	clientinfo = clientinfo .. "clientuserid="..data.value.clientuserid.value.."&"
end
if data.value.clientip.value ~= "" then
	clientinfo = clientinfo .. "clientip="..data.value.clientip.value.."&"
end
%>

<% local header_level = htmlviewfunctions.displaysectionstart(data, page_info) %>
<% htmlviewfunctions.displaypagination(pagedata, page_info) %>
<table id="loglist" class="tablesorter">
<thead>
<tr>
<% local checkhead = true
for i,watch in ipairs(subdata) do
	if watch.selected ~= "t" then
		checkhead = false
		break
	end
end %>
		<th><input type="checkbox" id="chkHead"<% if checkhead then %> checked<% end %>></th>
		<th>Timestamp</th>
		<th>Source</th>
		<th>Client IP</th>
		<th>User ID</th>
		<th>Size</th>
		<th>Sus</th>
		<th>Den</th>
		<th>Byp</th>
		<th>Score</th>
		<th>Reason</th>
		<th>URL</th>
		<th>Bad Words</th>
	</tr>
</thead>
<tbody>
<% for i,watch in ipairs(subdata) do
	local a,b = math.modf((i/2))
	local mark = ''
	if watch.selected == "t" then mark=' class="selected"' end
	local time = {}
	time.year, time.month, time.day, time.hour, time.min, time.sec =
		string.match(watch.logdatetime, "(%d+)%-(%d+)-(%d+)%s+(%d+):(%d+):(%d+)")
	time = os.time(time) %>
<tr<%= mark %>>
		<td><input class="chktbl" type="checkbox" id="<%= html.html_escape(watch.id) %>"<% if watch.selected == "t" then %> checked <% end %>></td>
		<td <% if data.value.focus.value == watch.logdatetime then %> style="font-weight:bold;" id="focus" <% end %> ><%= html.link{value = "viewweblog?"..clientinfo..
			"starttime="..os.date("%Y-%m-%d %H:%M:%S", time - 60*(tonumber(data.value.window.value)))..
			"&endtime="..os.date("%Y-%m-%d %H:%M:%S", time + 60*(tonumber(data.value.window.value)))..
			"&focus="..watch.logdatetime,
			label=watch.logdatetime} %></td>
		<td> <%= html.html_escape(watch.sourcename) %></td>
		<td <% if data.value.clientip.value == watch.clientip then %> style="font-weight:bold;" <% end %> ><%= html.html_escape(watch.clientip) %></td>
		<td <% if data.value.clientuserid.value == watch.clientuserid then %> style="font-weight:bold;" <% end %> ><%= html.html_escape(watch.clientuserid) %></td>
		<td><%= html.html_escape(watch.bytes) %></td>
		<td><span class="hide"><%= html.html_escape(watch.badyesno) %></span><% if watch.badyesno ~= "0" then %><img src='<%= html.html_escape(page_info.wwwprefix..page_info.staticdir) %>/dodgy.png' width='13' height='13'><% end %></td>
		<td><span class="hide"><%= html.html_escape(watch.deniedyesno) %></span><% if watch.deniedyesno ~= "0" then %> <img src='<%= html.html_escape(page_info.wwwprefix..page_info.staticdir) %>/denied.png' width='13' height='13'><% end %></td>
		<td><span class="hide"><%= html.html_escape(watch.bypassyesno) %></span><% if watch.bypassyesno ~= "0" then %> <img src='<%= html.html_escape(page_info.wwwprefix..page_info.staticdir) %>/bypass.png' width='13' height='13'><% end %></td>
		<td><%= html.html_escape(watch.score) %></td>
		<td<% if (watch.shortreason and watch.shortreason ~= "" and watch.reason and watch.reason ~= "") then %> title="<%= html.html_escape(watch.reason) %>"<% end %>>
			<% if (watch.shortreason and watch.shortreason ~= "") then %>
				<%= html.html_escape(watch.shortreason) %>
			<% elseif (watch.reason and watch.reason ~= "") then %>
				<%= html.html_escape(watch.reason) %>
			<% end %>
		</td>
		<td title="<%= html.html_escape(watch.uri) %>"><% highlight_uri=html.html_escape(watch.shorturi or watch.uri)
		if watch.wordloc ~= nil then
			if string.find(watch.wordloc, "|") then
				badwords = format.string_to_table(watch.wordloc, "|")
				for key,wrd in ipairs(badwords) do
					highlight_uri = string_highlight(highlight_uri, wrd, "yellow", "red")
				end
			else
				highlight_uri = string_highlight(highlight_uri, watch.wordloc, "yellow", "red")
			end
		end %>
		<%= highlight_uri %></td>
		<td><%= watch.wordloc %></td>
</tr>
<% end %>
</tbody>
</table>
<% htmlviewfunctions.displaypagination(pagedata, page_info) %>
<% htmlviewfunctions.displaysectionend(header_level) %>

<% if data.errtxt then %>
<p class="error"><%= html.html_escape(data.errtxt) %></p>
<% end %>
<% if #data.value.log.value == 0 then %>
<p>No results, try adjusting search parameters</p>
<% end %>

<% if viewlibrary.check_permission("downloadweblog") then
data.label = "Download Results"
data.action = "downloadweblog"
data.option = "Download"
data.value.log = nil
data.value.window = nil
data.value.focus = nil
data.value.viewtype = cfe({ value="stream" })
for n,v in pairs(data.value) do
	v.type = "hidden"
	v.checked = nil
end
htmlviewfunctions.displayitem(data, page_info, 0)
end %>
