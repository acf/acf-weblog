<% local data, viewlibrary, page_info, session = ...
htmlviewfunctions = require("htmlviewfunctions")
html = require("acf.html")
%>

<script type="text/javascript">
	if (typeof jQuery == 'undefined') {
		document.write('<script type="text/javascript" src="<%= html.html_escape(page_info.wwwprefix) %>/js/jquery-latest.js"><\/script>');
	}
</script>

<script type="text/javascript">
	if (typeof $.tablesorter == 'undefined') {
		document.write('<script type="text/javascript" src="<%= html.html_escape(page_info.wwwprefix) %>/js/jquery.tablesorter.js"><\/script>');
	}
</script>

<script type="text/javascript">
	$(document).ready(function() {
		$("#sources").tablesorter({headers: {0:{sorter: false}}, widgets: ['zebra']});
	});
</script>

<% htmlviewfunctions.displaycommandresults({"deletesource", "editsource", "testsource", "createsource", "importlogs"}, session) %>

<% local header_level = htmlviewfunctions.displaysectionstart(data, page_info) %>
<table id="sources" class="tablesorter"><thead>
	<tr>
		<th>Action</th>
		<th>Name</th>
		<th>Enabled</th>
		<th>Source</th>
		<th>Method</th>
	</tr>
</thead><tbody>
<% local sourcename = cfe({ type="hidden", value="" }) %>
<% local redir = cfe({ type="hidden", value=page_info.orig_action }) %>
<% for i,source in ipairs(data.value) do %>
	<tr>
		<td>
		<%
		sourcename.value = source.sourcename
		htmlviewfunctions.displayitem(cfe({type="link", value={sourcename=sourcename, redir=redir}, label="", option="Edit", action="editsource"}), page_info, -1)
		htmlviewfunctions.displayitem(cfe({type="form", value={sourcename=sourcename}, label="", option="Delete", action="deletesource" }), page_info, -1)
		htmlviewfunctions.displayitem(cfe({type="form", value={sourcename=sourcename}, label="", option="Test", action="testsource" }), page_info, -1)
		%>
		</td>
		<td><%=  html.html_escape(source.sourcename) %></td>
		<td><%= html.html_escape(tostring(source.enabled)) %></td>
		<td><%= html.html_escape(source.source) %></td>
		<td><%= html.html_escape(source.method) %></td>
	</tr>
<% end %>
</tbody>
</table>

<% if #data.value == 0 then %>
<p>No sources found</p>
<% end %>
<% htmlviewfunctions.displayinfo(data) %>

<% htmlviewfunctions.displayitem(cfe({type="link", value={redir=redir}, label="Create New Source", option="Create", action="createsource"}), page_info, 0) %>
<% htmlviewfunctions.displayitem(cfe({type="form", value={}, label="Import Logs", option="Import", action="importlogs" }), page_info, 0) %>

<% htmlviewfunctions.displaysectionend(header_level) %>
