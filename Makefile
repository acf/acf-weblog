APP_NAME=weblog
PACKAGE=acf-$(APP_NAME)
VERSION=0.11.1

CRON_FILE=weblogimport

IMAGE_DIST=bypass.png \
	denied.png \
	dodgy.png \

APP_DIST=weblog-* \
	weblog.roles \
	weblog.menu \

EXTRA_DIST=README Makefile config.mk

DISTFILES=$(CRON_FILE) $(IMAGE_DIST) $(APP_DIST) $(EXTRA_DIST)

TAR=tar

P=$(PACKAGE)-$(VERSION)
tarball=$(P).tar.bz2
install_dir=$(DESTDIR)/$(appdir)/$(APP_NAME)
cron_dir=$(DESTDIR)/etc/periodic/daily
static_dir=$(DESTDIR)/${wwwdir}/skins/static

all:
clean:
	rm -rf $(tarball) $(P)

dist: $(tarball)

install:
	mkdir -p "$(install_dir)" "$(cron_dir)" "$(static_dir)"
	cp -a $(APP_DIST) "$(install_dir)"
	cp -a $(CRON_FILE) "$(cron_dir)"
	cp -a $(IMAGE_DIST) "$(static_dir)"

$(tarball):	$(DISTFILES)
	rm -rf $(P)
	mkdir -p $(P)
	cp -a $(DISTFILES) $(P)
	$(TAR) -jcf $@ $(P)
	rm -rf $(P)

# target that creates a tar package, unpacks is and install from package
dist-install: $(tarball)
	$(TAR) -jxf $(tarball)
	$(MAKE) -C $(P) install DESTDIR=$(DESTDIR)
	rm -rf $(P)

include config.mk

.PHONY: all clean dist install dist-install
